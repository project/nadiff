
Node access differences

Displays all nodes are not represented in the node access database table and
provides a function to repair the table.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

Installation
--------------------------------------------------------------------------------
Copy the Node access differences module folder to your module directory and
then enable on the admin modules page.

USAGE
--------------------------------------------------------------------------------
1. Enable the module permission:
   Administer -> User management -> Permissions
   -> nadiff module -> access nadiff

2. Call the Node access differences utility:
   Administer -> Site configuration -> Node access differences

Maintainer
--------------------------------------------------------------------------------
Quiptime Group
Siegfried Neumann
www.quiptime.com
quiptime [ at ] gmail [ dot ] com