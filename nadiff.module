<?php
// $Id:

/**
 * @file
 * Displays all nodes are not represented in the node_access table and provides a function to repair the table.
 *
 *   Features:
 *   - Repair the node_access table
 *   - Delete the system variable node_access_needs_rebuild.
 */

/**
 * Implementation of hook_menu().
 */
function nadiff_menu() {
  $items = array();

  $items['admin/settings/nadiff'] = array(
    'title' => 'Node access differences',
    'description' => 'Displays all nodes are not represented in the node access database table and provides a function to repair the table.',
    'page callback' => 'nadiff_summary',
    'page arguments' => array(3),
    'access arguments' => array('access nadiff'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/settings/nadiff/solve'] = array(
    'title' => 'Nadiff fix sys var',
    'description' => 'Delete the system variable node_access_needs_rebuild.',
    'page callback' => 'drupal_get_form',
    'page arguments'   => array('nadiff_fix_system_var_form'),
    'access arguments' => array('access nadiff'),
    'type' => MENU_SUGGESTED_ITEM,
    'menu_name' => 'devel',
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function nadiff_perm() {
  return array('access nadiff');
}

/**
 * Implementation of hook_form_alter().
 */
function nadiff_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'node_configure':
      $description = t('The node access differences (Nadiff) tool can help you are a problem with "The content access permissions need to be rebuilt." after the permissions rebuild.') .'<br />';
      $description .= t('The tool provides an overview to nodes with access differences an can used in the repair modus.');
      $form['nadiff'] = array(
        '#type' => 'fieldset',
        '#title' => t('Nadiff tool'),
        '#description' => $description,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#weight' => 0
      );
      $form['nadiff']['link'] = array(
        '#type' => 'markup',
        '#value' => t('The tool') .': '. l('Nadiff', 'nadiff')
      );
      break;
  }
}

/**
 * Menu callback.
 *
 *   Check differences between tables node and node_access.
 */
function nadiff_summary($modus = FALSE) {
  global $repair_call;

  $output = '';
  $list_rows = 50;
  $title = t('Node access differences');

  $sql = "SELECT n.nid, n.title, n.uid, n.type, n.created, n.status FROM {node} n WHERE n.nid NOT IN (SELECT na.nid FROM {node_access} na)";
  $sql_cnt = "SELECT COUNT(DISTINCT(nid)) FROM {node} WHERE nid NOT IN (SELECT nid FROM {node_access})";

  $sort_title = array('data' => t('Title'), 'field' => 'title');
  $sort_node_id = array('data' => t('Node ID'), 'field' => 'nid');
  $sort_type = array('data' => t('Type'), 'field' => 'type');
  $sort_created = array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc');
  $sort_status = array('data' => t('Status'), 'field' => 'status');
  $sort_uid = array('data' => t('User ID'), 'field' => 'uid');

  $header = array($sort_title, $sort_node_id, $sort_type, $sort_created, $sort_status, $sort_uid, t('Operations'));

  // Initiate sortable feature.
  $node_sort = tablesort_sql($header, NULL, 'node');
  $sql .= $node_sort;
  $result = pager_query($sql, $list_rows, 0, $sql_cnt);

  $rows = array();
  while ($nodes = db_fetch_object($result)) {
    $recall[] = $nodes->nid;
    $rows[] = array(
      l($nodes->title, 'node/'. $nodes->nid),
      $nodes->nid,
      $nodes->type,
      format_date($nodes->created, 'medium'),
      $nodes->status,
      $nodes->uid,
      l(t('Edit'), 'node/'. $nodes->nid .'/edit', array('query' => array('destination' => 'nadiff'))),
    );

    // Call module invoke to get a list of grants and then write them to the database.
    // This may fix the node_access table. But this repair does not always work.
    if ($modus == 'repair') {
      $sencer = node_load($nodes->nid, NULL, FALSE);
      node_access_acquire_grants($sencer);
    }
  }

  if (!$repair_call && $modus === 'repair' && count($rows) > 0) {
    $repair_call = 1;
    return nadiff_summary('repair');
  }

  if (count($rows) > 0) {
    if (!$repair_call) {
      $output .= '<p>'. t('This is an initial call. See the current differences.') .'</p>';
      $output .= '<p>'. t('You can call the list in the repair mode: <a href="@repair-mode-url">@repair-mode</a>', array('@repair-mode-url' => url('admin/settings/nadiff/repair'), '@repair-mode' => t('Repair mode'))) .'<br />';
      $output .= t('You can edit each node individually. Saving the node repaired their entry in the node access table.') .'</p>';
    }
    else {
      $output .= '<p>'. t('This is an repair call. These nodes in could not be repaired in the node access database table.') .'<br />';
      $output .= t('Check the configuration of permissions in the access modules.'). '</p>';
    }

    $output .= theme('table', $header, $rows, array(), NULL, 'node');
    $output .= theme('pager', NULL, $list_rows, 0, array(), 9);
  }
  else {
    $message = t('No differences between database tables <em>node</em> and <em>node access</em> exists.') .'<br />';

    $node_access_needs_rebuild = variable_get('node_access_needs_rebuild', FALSE);
    if ($node_access_needs_rebuild) {
      $message .= '<p>'. t('Is permanently displayed the message "The content access permissions need to be rebuilt. ..."?') .'<br />';
      $message .= t('<a href="@delete-url">Delete</a> the responsible system variable.', array('@delete-url' => url('admin/settings/nadiff/solve'))) .'<br />';
      $message .= t('Note: Delete the responsible system variable only if there are not problems with the node access database table.') .'</p>';
    }
    
    drupal_set_message($message, 'status');
    $title = t('No node access differences exists');
  }

  drupal_set_title($title);

  return $output;
}

/**
 * Menu callback.
 *
 *   Provides a form to delete the system variable node_access_needs_rebuild.
 */
function nadiff_fix_system_var_form() {
  $form = array();

  $form['nadiff'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fix system variable'),
    '#description' => t('Delete the system variable <strong>node_access_needs_rebuild</strong><br />to fix the problem is permanently displayed the message "The content access permissions need to be rebuilt. ...".'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 0
  );
  $form['fix_sys_var'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );
  $form['nadiff_fix'] = array(
    '#type' => 'submit',
    '#value' => t('Delete the system variable'),
  );
  $form['#submit'][] = 'nadiff_fix_system_var_form_submit';

  return $form;
}

/**
 * Submit callback.
 *
 *   Delete the system variable node_access_needs_rebuild.
 */
function nadiff_fix_system_var_form_submit($form, $formstate) {
  if($formstate['values']['fix_sys_var'] == TRUE) {
    variable_del('node_access_needs_rebuild');
  }
  $form_state['redirect'] = drupal_goto('admin/settings/nadiff');
}
